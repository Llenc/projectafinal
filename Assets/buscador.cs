using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class buscador : MonoBehaviour
{
  
    public GameObject player;
    public float speed;
    private Vector3 offset;

    void FixedUpdate(){  
         offset = player.transform.position;     
        
        transform.position = Vector3.MoveTowards(transform.position, offset, speed);        
    
    }
}
