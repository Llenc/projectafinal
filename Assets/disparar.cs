using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparar : MonoBehaviour{

    public GameObject arrow;

    void Update(){
        if(Input.GetKeyDown(KeyCode.UpArrow) && (gameObject.tag == "up")){
            GameObject g = (GameObject) Instantiate(arrow, transform.position, (transform.rotation));
            Physics2D.IgnoreCollision(g.GetComponent<Collider2D>(), transform.parent.GetComponent<Collider2D>());
        }
        if(Input.GetKeyDown(KeyCode.DownArrow) && (gameObject.tag == "down")){
            GameObject g = (GameObject) Instantiate(arrow, transform.position, transform.rotation);
            Physics2D.IgnoreCollision(g.GetComponent<Collider2D>(), transform.parent.GetComponent<Collider2D>());
        }
        if(Input.GetKeyDown(KeyCode.RightArrow) && (gameObject.tag == "right")){
            GameObject g = (GameObject) Instantiate(arrow, transform.position, transform.rotation);
            Physics2D.IgnoreCollision(g.GetComponent<Collider2D>(), transform.parent.GetComponent<Collider2D>());
        }
        if(Input.GetKeyDown(KeyCode.LeftArrow) && (gameObject.tag == "left")){
            GameObject g = (GameObject) Instantiate(arrow, transform.position, transform.rotation);
            Physics2D.IgnoreCollision(g.GetComponent<Collider2D>(), transform.parent.GetComponent<Collider2D>());
        }
    }

}
