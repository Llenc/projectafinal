using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerMain : MonoBehaviour{

    public float Pspeed = 5.0f;
    public float Nspeed = -5.0f;
    public float HP = 3f;
    public Text text;

    void Start(){
        updateText();
    }

    void Update()
    {
        if (HP <= 0){
            Destroy(gameObject);
        }
    }

    void FixedUpdate(){

        if (Input.GetKey(KeyCode.W)){
            GetComponent<Animator>().SetBool( "up" , true);
            GetComponent<Animator>().SetBool( "down" , false);
            GetComponent<Animator>().SetBool( "left" , false);
            GetComponent<Animator>().SetBool( "right" , false);
        
            transform.Translate(0, Pspeed, 0);
        }
        if (Input.GetKey(KeyCode.A)){
            GetComponent<Animator>().SetBool( "up" , false);
            GetComponent<Animator>().SetBool( "down" , false);
            GetComponent<Animator>().SetBool( "left" , true);
            GetComponent<Animator>().SetBool( "right" , false);

            transform.Translate(Nspeed, 0, 0);
        }
        if (Input.GetKey(KeyCode.S)){
            GetComponent<Animator>().SetBool( "up" , false);
            GetComponent<Animator>().SetBool( "down" , true);
            GetComponent<Animator>().SetBool( "left" , false);
            GetComponent<Animator>().SetBool( "right" , false);

            transform.Translate(0, Nspeed, 0);
        }
        if (Input.GetKey(KeyCode.D)){
            GetComponent<Animator>().SetBool( "up" , false);
            GetComponent<Animator>().SetBool( "down" , false);
            GetComponent<Animator>().SetBool( "left" , false);
            GetComponent<Animator>().SetBool( "right" , true);

            transform.Translate(Pspeed, 0, 0);
        }
                
    }
    void OnCollisionEnter2D(Collision2D coll){
        if (coll.gameObject.tag == "arrowe"){
            HP--;

            updateText();
        }
        if(coll.gameObject.tag == "vida"){
            HP++;
            Destroy(coll.gameObject);

            updateText();
        }
        if(coll.gameObject.tag == "speed"){
            if (Pspeed < 0.20f){
                Pspeed += 0.02f;
                Nspeed -= 0.02f;
            }

            Destroy(coll.gameObject);
        }
    }
    void updateText(){
        text.text = "HP = " + HP;
    }
}
