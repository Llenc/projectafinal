using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wall : MonoBehaviour
{
   void OnCollisionEnter2D(Collision2D coll){
        if ((coll.gameObject.tag != "Player") && (coll.gameObject.tag != "Wall")) {
            Destroy(coll.gameObject);
        }
    }
}
