using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemydispara : MonoBehaviour
{
    public GameObject arrow;

    public float interval = 5;

    void Start(){
        InvokeRepeating("Fire", interval,interval);
    }

    void Fire(){
        GameObject g = (GameObject) Instantiate(arrow, transform.position, Quaternion.identity);
        Physics2D.IgnoreCollision(g.GetComponent<Collider2D>(), transform.parent.GetComponent<Collider2D>());
    }

}
