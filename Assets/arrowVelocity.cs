using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowVelocity : MonoBehaviour{

    public float speed = 5.0f;

    void FixedUpdate(){
        transform.Translate(0, speed * Time.deltaTime, 0);     
    }
}
